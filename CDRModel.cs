﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDR_Alert
{
    class CDRModel
    {
        public DateTime STARTCALL_DATETIME { get; set; }
        public string OUT_DIALED_NUMBER { get; set; }
        public string OUT_ANI { get; set; }
        public string OUT_CONN_TYPE { get; set; }
        public string IN_ORG_NUMBER { get; set; }
        public string IN_DEST_NUMBER { get; set; }
        public string CONNECTED_MINUTES { get; set; }
        public string DISCONNECTION_REASON { get; set; }
        public string CHANNEL_NUMBER { get; set; }
        public string DEVICE_NAME { get; set; }
        public string SIP_SERVER { get; set; }
        public string DIALER_NAME { get; set; }

    }
}
