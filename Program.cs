﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CDR_Alert
{
    class Program
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        static async Task Main(string[] args)
        {
            var date = $"CDR-{DateTime.Today.ToString("MM-dd-yy")}.csv";


            var cdrfile = Path.Combine(Tools.ReadSetting("cdr_dir"), date);
            var cdrList = new List<CDRModel>();

            using (var fs = new FileStream(cdrfile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    string header = sr.ReadLine(); //header
                    while (!sr.EndOfStream)
                    {
                        var fields = sr.ReadLine().Split(',');

                        var model = new CDRModel()
                        {
                            STARTCALL_DATETIME = DateTime.Parse(fields[0].Trim().Trim('"')),
                            OUT_DIALED_NUMBER = fields[1].Trim().Trim('"'),
                            OUT_ANI = fields[2].Trim().Trim('"'),
                            OUT_CONN_TYPE = fields[3].Trim().Trim('"'),
                            IN_ORG_NUMBER = fields[4].Trim().Trim('"'),
                            IN_DEST_NUMBER = fields[5].Trim().Trim('"'),
                            CONNECTED_MINUTES = fields[6].Trim().Trim('"'),
                            DISCONNECTION_REASON = fields[7].Trim().Trim('"'),
                            CHANNEL_NUMBER = fields[8].Trim().Trim('"'),
                            DEVICE_NAME = fields[9].Trim().Trim('"'),
                            SIP_SERVER = fields[10].Trim().Trim('"'),
                            DIALER_NAME = fields[11].Trim().Trim('"')
                        };
                        cdrList.Add(model);
                        // Your code here
                    }
                }
            }

            Logger.Debug($"CDR Records Found: {cdrList.Count}");
            List<CDRModel> selecteddata = cdrList.OrderByDescending(t => t.STARTCALL_DATETIME).Take(100).ToList();
            var trouble = "";

            trouble += TroubleMakers.HighNoResults(selecteddata);
            trouble += TroubleMakers.HighBadResults(selecteddata);



            if (trouble != "")
            {
                trouble = "<html> <body> <ul> " + trouble + " </ul> </body> </html>";
                await SendEmail.Send(Tools.ReadSetting("DialerName") + " CDR Alert!", trouble);
                Logger.Info($"Email Alert Sent: {trouble}");
            }
            else
            {
                Logger.Info($"Trouble Not Detected");
            }

        }
    }
}
