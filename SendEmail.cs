﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CDR_Alert
{
    class SendEmail
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static async Task Send(string Subject, string Body)
        {
            try
            {
                var message = new MailMessage();
                message.To.Add(ConfigurationManager.AppSettings["EmailAlerts"]);
                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"]);  // replace with valid value
                message.Subject = Subject;
                message.Body = string.Format(Body, "CDR Alert", ConfigurationManager.AppSettings["EmailFrom"], Body);
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    await smtp.SendMailAsync(message);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }


        }
    }
}
