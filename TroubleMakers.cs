﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDR_Alert
{
    class TroubleMakers
    {
        public static string HighNoResults (List<CDRModel> data)
        {
            var numberOfResults = data.Where(x => x.OUT_CONN_TYPE == "" && x.IN_ORG_NUMBER == "").Count();

            if(numberOfResults / (double)data.Count > .5)
            {
                return "<li>Over 50% of connection types are returning blank</li>";
            }
            return "";
        }

        public static string HighBadResults(List<CDRModel> data)
        {
            var result = "";
            var numberOfResults = data.Where(x => x.OUT_CONN_TYPE == "EDU_CR_NORB" && x.IN_ORG_NUMBER == "").Count();
            if (numberOfResults / (double)data.Count > .1)
            {
                result = "<li>Over 10% of connection types are returning No Ringback</li>";
            }
            numberOfResults = data.Where(x => x.OUT_CONN_TYPE == "EDU_CR_CALL_FAILED" && x.IN_ORG_NUMBER == "").Count();
            if (numberOfResults / (double)data.Count > .2)
            {
                result += "<li>Over 20% of connection types are returning Call Failed</li>";
            }
            return result;
        }
    }
}
